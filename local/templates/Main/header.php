<!DOCTYPE html>
<html lang="ru">
	<head>
        <?php
        $APPLICATION->ShowHEAD();
        $APPLICATION->ShowPanel();
        use Bitrix\Main\Page\Asset;

        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/libs.min.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/main.css");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/common.js");
        ?>
		<meta charset="UTF-8">
		<meta name="viewport", content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1.0, user-scalable=no">
		<meta http-equiv="X-UA-Compatible", content="ie=edge">

		<meta property="og:url" content="URL">
		<meta property="og:type" content="website">
		<meta property="og:title" content="Заголовок">
		<meta property="og:description" content="Описание">
		<meta property="og:site_name" content="Название сайта">
		<meta property="og:locale" content="ru_RU">
		<meta property="og:image" content="Картинка страницы">
		<meta property="og:image:alt" content="Описание картинки">
		<meta property="fb:app_id" content="966242223397117" />
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@аккаунт компании">
		<meta name="twitter:title" content="Заголовок">
		<meta name="twitter:description" content=“Описание">
		<meta name="twitter:image:src" content="Картинка страницы">

		<title> HawkingSchool. Страница курса </title>

		  <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		  <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/livingston-css3-mediaqueries-js/1.0.0/css3-mediaqueries.min.js"></script><![endif]-->
	</head>
	<body data-aos="fade">

		<main class="wrapper">
			<header class="header" data-aos="fade-down">
				<div class="container">
					<nav class="header__nav">
						<div>
							<a href="index.html" class="header__logo">
								<img src="images/LOGO.png" alt="" class="header__logo_img">
							</a>
						</div>

						<div class="gamburger js-gamburger">
							<span class="gamburger__line js-gamburger-line gamburger__line_top js-line-top"></span>
							<span class="gamburger__line js-gamburger-line gamburger__line_center js-line-center"></span>
							<span class="gamburger__line js-gamburger-line gamburger__line_bottom js-line-bottom"></span>
						</div>

						<div class="menu mobile closed js-menu">
							<div class="menu__item">
								<a href="index.html#about" class="menu__link js-scroll-to">О нас</a>
							</div>
							<div class="menu__item dropdown js-drop course-link js-dropdown">
								<span class="menu__link menu__link_arrow">Курсы</span>
								<div class="dropdown__wrap js-dropdown__wrap">
									<div class="menu__item">
										<a href="#" class="menu__link"><span>Менеджер проектов</span></a>
									</div>
									<div class="menu__item">
										<a href="#" class="menu__link"><span>SEO</span><span class="soon">СКОРО!</span></a>
									</div>
									<div class="menu__item">
										<a href="#" class="menu__link"><span>Контекст</span><span class="soon">СКОРО!</span></a>
									</div>
									<div class="menu__item link-1c">
										<a href="#" class="menu__link"><span>Программист 1С&nbsp;Битрикс</span><span class="soon">СКОРО!</span></a>
									</div>
								</div>
							</div>
							<div class="menu__item contacts-link">
								<a href="#contacts" class="menu__link js-scroll-to">Контакты</a>
							</div>
							<div class="menu__item menu__item_new">
								<a href="#" class="menu__link menu__link_new"><span>2.0</span></a>
							</div>

							<div class="header__actions">
								<a href="tel:+74922213239" class="header__phone">+7 (4922) 21-32-39</a>
								<span class="header__qestion"><a href="#modalQuestion" data-popup>Задать вопрос</a></span>
							</div>
						</div>
						<div class="header__actions-desktop">
							<a href="tel:+74922213239" class="header__phone">+7 (4922) 21-32-39</a>
                            <span class="header__qestion"><a href="#modalQuestion" data-popup>Задать вопрос</a></span>
                        </div>
					</nav>
				</div>
			</header>
